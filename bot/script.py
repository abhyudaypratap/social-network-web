import random
import string
import requests

from pyhunter import PyHunter

from bot import config

hunter = PyHunter(config.HUNTER_API_KEY)

base_url = config.BASE_URL
max_posts = config.MAX_POSTS_PER_USER
max_likes = config.MAX_LIKES_PER_USER
users_list = []


def fetch_valid_email_ids(limit):
    emails = []
    response = hunter.domain_search(config.TEST_DOMAIN, limit=limit)
    if 'emails' in response:
        for data in response['emails']:
            emails.append(data['value'])
    return emails


def create_users():
    valid_emails = config.VALID_EMAILS
    url = base_url + "auth/register"
    payload = {'email': '', 'password': 'password'}
    users = {}
    for email in valid_emails:
        payload['email'] = email
        response = requests.post(url, data=payload)
        users[email] = response.json()['auth_token']
    return users


def create_posts(users):
    url = base_url + "posts/"
    payload = {'post_type': 'text'}
    posts = {}
    for email, token in users.items():
        headers = {
            "content-type": "application/json",
            "Authorization": "Token " + token,
        }
        posts[email] = []
        payload['body'] = "".join([random.choice(string.ascii_letters) for i in range(1, 15)])
        for i in range(random.randint(1, max_posts)):
            print(payload)
            response = requests.post(url, headers=headers, json=payload)
            posts[email].append(response.json()['id'])
    return posts


def get_post_list_order(posts, updated_posts, post_list):
    if len(updated_posts) == 0:
        return post_list
    max_key = max(updated_posts, key=lambda x: len(set(updated_posts[x])))
    new_posts = posts.copy()
    new_posts.pop(max_key)
    updated_posts.pop(max_key)
    post_ids = [item for key in new_posts.keys() for item in new_posts[key]]
    post_list.append({max_key: post_ids})
    return get_post_list_order(posts, updated_posts, post_list)


def like_posts(posts, users):
    payload = {'reaction_type': 1}
    post_list = get_post_list_order(posts, posts, list())
    headers = {
        "content-type": "application/json",
    }
    for user in post_list:
        count = 0
        for email, post_ids in user.items():
            for post_id in post_ids:
                headers["Authorization"] = "Token " + users[email]
                url = base_url + "posts/" + post_id + "/reactions/"
                requests.post(url, headers=headers, json=payload)
                count += 1
                if count >= max_likes:
                    break
            if count >= max_likes:
                    break


def demonstrate_apis():
    users = create_users()
    posts = create_posts(users=users)
    like_posts(posts=posts, users=users)
