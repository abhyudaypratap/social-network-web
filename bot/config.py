BASE_URL = 'http://localhost:8000/api/'
NUMBER_OF_USERS = 10
MAX_POSTS_PER_USER = 10
MAX_LIKES_PER_USER = 50
TEST_DOMAIN = 'stripe.com'
HUNTER_API_KEY = '4ac9e8c9f589ff87989542cbd185ca31ac174dfb'

VALID_EMAILS = [
    'max.lahey@stripe.com',
    'owen.coutts@stripe.com',
    'larry@stripe.com',
    'daniel.heffernan@stripe.com',
    'piruze.sabuncu@stripe.com',
    'charles.francis@stripe.com',
    'rasmus.rygaard@stripe.com',
    'guillaume.princen@stripe.com',
    'julia@stripe.com',
    'joe.cruttwell@stripe.com',
    'christian.anderson@stripe.com',
    'felix.huber@stripe.com',
    'larry.ullman@stripe.com',
    'john.wang@stripe.com',
    'stephen.wan@stripe.com',
    'mark@stripe.com',
    'jack.flintermann@stripe.com',
    'michael.manapat@stripe.com',
    'amber.feng@stripe.com',
    'kiran@stripe.com'
]
