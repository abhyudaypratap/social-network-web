from django.db.models import F

# Social Network Stuff
from social_network.posts.models import Post, Comment, Reaction


def get_post_by_id(post_id):
    """
    get post by id
    """
    post = Post.objects.filter(id=post_id).first()
    if post:
        return post
    return None


def get_comment_by_id(comment_id):
    """
    get comment by id
    """
    comment = Comment.objects.filter(id=comment_id).first()
    if comment:
        return comment
    return None


def get_all_comments(post_id):
    """
    Fetch all comments
    """
    comments = Comment.objects.filter(post=post_id)
    return comments


def get_reaction_by_id(reaction_id):
    """
    fetch reaction by id
    """
    reaction = Reaction.objects.filter(id=reaction_id).first()
    if reaction:
        return reaction
    return None


def create_post(body, post_type, owner):
    """
    Create a post
    """
    post = Post.objects.create(
        body=body,
        post_type=post_type,
        owner=owner,
    )
    return post


def create_comment(post, body, created_by):
    """
    Create a comment
    """
    comment = Comment.objects.create(
        post=post,
        created_by=created_by,
        body=body,
    )
    Post.objects.filter(id=comment.post.id).update(comment_count=F('comment_count') + 1)
    return comment


def reaction_count(post_id):
    """
    Get reactions count for post
    """
    like_count = Reaction.objects.filter(post_id=post_id, reaction_type=1).count()
    dislike_count = Reaction.objects.filter(post_id=post_id, reaction_type=2).count()
    return (like_count, dislike_count)


def create_update_reaction(post, reaction_type, created_by):
    """
    Create a reaction instance
    """
    data = {'created': True, 'error': False}
    reaction, created = Reaction.objects.get_or_create(post=post, created_by=created_by)
    reaction.reaction_type = reaction_type
    reaction.save()
    data['created'] = created

    return data
