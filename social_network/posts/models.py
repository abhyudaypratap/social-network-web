# Third Party Stuff
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# Social Network Clone Stuff
from social_network.base.models import UUIDModel
from social_network.base.utils.storage import get_file_path


class Post(UUIDModel):
    TYPE_CHOICES = (
        ('photo', "Photo"),
        ('text', "Text"),
    )

    body = models.TextField()
    post_type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='owned_posts')
    created_on = models.DateTimeField(default=timezone.now)
    photo = models.FileField(upload_to=get_file_path,
                             max_length=500, null=True, blank=True,
                             verbose_name=_("post photo"))
    last_edit_date = models.DateField(_('Last Edit Date'), null=True, blank=True)
    reactions_count = models.IntegerField(default=0)
    comments_count = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        ordering = ('-created_on', )

    def __str__(self):
        return str(self.id)


class Comment(UUIDModel):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    body = models.TextField()
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='comments', null=True)

    class Meta:
        verbose_name = _('comment')
        verbose_name_plural = _('comments')
        ordering = ('-created_on', )

    def __str__(self):
        return str(self.id)


class Reaction(UUIDModel):

    TYPE_CHOICES = (
        (1, "Like"),
        (2, "Dislike"),
    )
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='reactions')
    reaction_type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    created_on = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
                                   related_name='reactions', null=True)

    class Meta:
        verbose_name = _('reaction')
        verbose_name_plural = _('reactions')
        ordering = ('-created_on', )

    def __str__(self):
        return str(self.id)
