# Third Party Stuff
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action

# Social Network Clone Stuff
from social_network.base import response
from social_network.users.auth.backends import UserTokenAuthentication

from . import serializers, services


class PostViewSet(viewsets.GenericViewSet):

    serializer_class = serializers.PostSerializer
    authentication_classes = (UserTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get_object(self, request, post_id):
        post = services.get_post_by_id(post_id=post_id)
        serializers_data = serializers.PostSerializer(post).data
        return response.Ok(serializers_data)

    @action(methods=['POST', ], detail=False)
    def create(self, request):
        post_data = request.data
        data = dict(post_data.items())
        data['owner'] = request.user.id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        post = services.create_post(**serializer.validated_data)
        data = serializers.PostSerializer(post).data
        return response.Created(data)

    def partial_update(self, request, post_id):
        """Update a contact"""
        instance = services.get_post_by_id(post_id=post_id)
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Ok(serializer.data)


class CommentViewSet(viewsets.GenericViewSet):

    serializer_class = serializers.CommentSerializer
    authentication_classes = (UserTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get_comments(self, request, post_id):
        comments = services.get_all_comments(post_id=post_id)
        data = serializers.CommentSerializer(comments, many=True).data
        return response.Ok(data)

    def get_object(self, comment_id):
        comment = services.get_comment_by_id(comment_id=comment_id)
        serializers_data = serializers.CommentSerializer(comment).data
        return serializers_data

    @action(methods=['POST', ], detail=False)
    def create(self, request, post_id):
        post_data = request.data
        data = dict(post_data.items())
        data['post'] = post_id
        data['created_by'] = request.user.id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        comment = services.create_comment(**serializer.validated_data)
        data = serializers.CommentSerializer(comment).data
        return response.Created(data)

    def partial_update(self, request, post_id, comment_id):
        """Update a contact"""
        instance = services.get_comment_by_id(comment_id=comment_id)
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Ok(serializer.data)


class ReactionViewSet(viewsets.GenericViewSet):

    serializer_class = serializers.ReactionSerializer
    authentication_classes = (UserTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    @action(methods=['POST', ], detail=False)
    def create_update(self, request, post_id):
        post_data = request.data
        data = dict(post_data.items())
        data['post'] = post_id
        data['created_by'] = request.user.id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        data = services.create_update_reaction(**serializer.validated_data)
        return response.Created(data)
