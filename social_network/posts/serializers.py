from rest_framework import serializers

from . import models


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Post
        fields = ["id", "body", "post_type", "photo", "owner", "last_edit_date",
                  "reactions_count", "comments_count"]


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Comment
        fields = ["id", "post", "body", "created_by"]


class ReactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Reaction
        fields = ["id", "post", "reaction_type", "created_by"]
