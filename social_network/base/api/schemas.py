# Third Party Stuff
from rest_framework.permissions import AllowAny
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.views import get_swagger_view

schema_view = get_schema_view(
    title='Social Network API',
    description='Object of this task is to create a simple REST API based social network in Django, and create a bot which will demonstrate functionalities of the API according to defined rules.',
    public=True,
    permission_classes=[AllowAny, ])

swagger_schema_view = get_swagger_view(title='Social Network API Playground')
