# Third Party Stuff
from django.contrib.auth import get_user_model, authenticate
from django.conf import settings
import clearbit

# Social Network Stuff
from social_network.base import exceptions as exc

clearbit.key = settings.CLEARBIT_API_KEY


def get_user_data(email):
    """
    clearbit.com/enrichment for getting additional data for the user
    """
    response = clearbit.Enrichment.find(email=email)
    data = {'first_name': '', 'last_name': ''}
    if 'person' in response and response['person'] is not None:
        if 'name' in response['person'] and response['person']['name'] is not None:
            data['first_name'] = response['person']['name']['givenName'] \
                if response['person']['name']['givenName'] else ''
            data['last_name'] = response['person']['name']['familyName'] \
                if response['person']['name']['familyName'] else ''

        data['bio'] = response['person']['bio']
        data['city'] = response['person']['geo']['city']
        data['country'] = response['person']['geo']['country']
        data['website'] = response['person']['site']
        data['facebook_handle'] = response['person']['facebook']['handle']
        data['github_handle'] = response['person']['github']['handle']
        data['linkedin_handle'] = response['person']['linkedin']['handle']
        data['twitter_handle'] = response['person']['twitter']['handle']

    return data


def get_and_authenticate_user(email, password):
    user = authenticate(username=email, password=password)
    if user is None:
        raise exc.WrongArguments("Invalid username/password. Please try again!")

    return user


def create_user_account(email, password):
    extra_fields = get_user_data(email=email)
    user = get_user_model().objects.create_user(
        email=email, password=password, **extra_fields
    )
    return user


def get_user_by_email(email: str):
    return get_user_model().objects.filter(email__iexact=email).first()
