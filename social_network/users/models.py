# Third Party Stuff
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.postgres.fields import CIEmailField
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

# Social Network Stuff
from social_network.base.models import UUIDModel
from social_network.base.utils.storage import get_file_path


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email: str, password: str, is_staff: bool, is_superuser: bool, **extra_fields):
        """Creates and saves a User with the given email and password.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff,
                          is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email: str, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email: str, password: str, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class User(AbstractBaseUser, UUIDModel, PermissionsMixin):
    first_name = models.CharField(_('First Name'), max_length=120, blank=True)
    last_name = models.CharField(_('Last Name'), max_length=120, blank=True)
    # https://docs.djangoproject.com/en/1.11/ref/contrib/postgres/fields/#citext-fields
    email = CIEmailField(_('email address'), unique=True, db_index=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text='Designates whether the user can log into this admin site.')

    is_verified = models.BooleanField('active', default=True,
                                      help_text='Designates whether this user should be treated as '
                                                'verified.')
    photo = models.FileField(upload_to=get_file_path,
                             max_length=500, null=True, blank=True,
                             verbose_name=_("profile photo"))
    phone_number = PhoneNumberField(blank=True, null=True, unique=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    # Social Information
    bio = models.CharField(_('Bio'), max_length=200, blank=True, null=True)
    city = models.CharField(_('City'), max_length=200, blank=True, null=True)
    country = models.CharField(_('Country'), max_length=200, blank=True, null=True)
    website = models.CharField(_('Website'), max_length=300, blank=True, null=True)
    facebook_handle = models.CharField(_('Facebook'), max_length=200, blank=True, null=True)
    github_handle = models.CharField(_('Github'), max_length=200, blank=True, null=True)
    linkedin_handle = models.CharField(_('Linkedin'), max_length=200, blank=True, null=True)
    twitter_handle = models.CharField(_('Twitter'), max_length=200, blank=True, null=True)

    USERNAME_FIELD = 'email'
    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        ordering = ('-date_joined', )

    def __str__(self):
        return str(self.id)

    def get_full_name(self) -> str:
        """Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self) -> str:
        """Returns the short name for the user.
        """
        return self.first_name.strip()
