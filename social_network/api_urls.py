# Third Party Stuff
from rest_framework.routers import DefaultRouter

# Social Network Stuff
from social_network.base.api.routers import SingletonRouter
from social_network.users.api import CurrentUserViewSet
from social_network.users.auth.api import AuthViewSet
from social_network.posts.api import PostViewSet, CommentViewSet, ReactionViewSet
from django.conf.urls import url

default_router = DefaultRouter(trailing_slash=False)
singleton_router = SingletonRouter(trailing_slash=False)

default_router = DefaultRouter(trailing_slash=False)
singleton_router = SingletonRouter(trailing_slash=False)

# Register all the django rest framework viewsets below.
default_router.register('auth', AuthViewSet, basename='auth')
singleton_router.register('me', CurrentUserViewSet, basename='me')

post_set = PostViewSet.as_view({
    'post': 'create',
})

update_post_set = PostViewSet.as_view({
    'get': 'get_object',
    'patch': 'partial_update',
})

comments_set = CommentViewSet.as_view({
    'get': 'get_comments',
    'post': 'create',
})

comment_set = CommentViewSet.as_view({
    'get': 'get_object',
    'patch': 'partial_update',
})

reaction_set = ReactionViewSet.as_view({
    'post': 'create_update',
})

urlpatterns_explicit = [
    url(r'^posts/$', post_set, name='posts'),
    url(r'^posts/(?P<post_id>[^/.]+)/$', update_post_set, name='posts'),
    url(r'^posts/(?P<post_id>[^/.]+)/comments/$', comments_set, name='comments'),
    url(r'^posts/(?P<post_id>[^/.]+)/comments/(?P<comment_id>[^/.]+)/$', comment_set, name='comment'),
    url(r'^posts/(?P<post_id>[^/.]+)/reactions/$', reaction_set, name='reaction'),
]

# Register all the django rest framework viewsets below.
default_router.register('auth', AuthViewSet, basename='auth')
singleton_router.register('me', CurrentUserViewSet, basename='me')

# Combine urls from both default and singleton routers and expose as
# 'urlpatterns' which django can pick up from this module.
urlpatterns = default_router.urls + singleton_router.urls + urlpatterns_explicit
